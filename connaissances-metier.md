# Module : Connaissances du métier

## Gestion d'une Infrastucture IT

* Les services et organisations de l'informatique
* Maintien en condition opérationnelle : la gestion des évènements, incidents, problèmes et changements suivant l'`ITIL`
* Outillage des processus de maintien en condition opérationnelle (ticketing, monitoring, sauvegarde, snapshots, gestion des configurations, etc... )
* Présentation d'une architecture pour une plateforme de services

__Objectifs :__

* Valider ses connaissances en culture générale autour de l'informatique et des métiers liés à la gestion des infrastructures informatiques
* Avoir une attitude professionelle, maîtriser son périmètre de responsabilité et d'intervention
* Savoir faire de la veille technologique
* Être capable de décrire une architecture de plateforme de service

## Outils de travail incontournables

* Gestion de son système d'exploitation
* Sauvegarde de ses données
* Les accès distants
* Versionning avec git
* Virtualisation système sur son ordinateur personnel

__Objectifs :__

* Savoir gérer et planifier la gestion de son ordinateur
* Savoir assurer une pérennité de la donnée au travers des sauvegarde et de l'utilisation des processus et outils
* Savoir utiliser les outils `git`, `ssh`, `rdesktop`
* Être capable de préparer et valider ses opérations système sur un lab virtualisé (partie système)


__Travail en autonomie sur ce module :__

* Remise aux normes de son ordinateur personnel
* Mise en place de sauvegardes semi-automatisées de son laptop
* Veille technologique : choix d'un outils à intégrer (ticketing, sauvegarde, monitoring, services internet, etc...) : __la solution retenue par l'étudiant__
* Definition de l'architecture de plateforme de services pour l'outil choisi

