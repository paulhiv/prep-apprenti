# le laptop

**Votre outil de travail** : il doit être toujours fonctionnel et rester fiable.
Dans la plupart des cas celui-ci vous est fourni et est maintenu par votre employeur. Il est en revanche assez courant de pouvoir choisir son système d'exploitation, de disposer des droits d'amin, voir même de gèrer sois même son laptop.

Il n'est pas malvenu (bien au contraire) de préciser vos habitudes et besoins lors d'un entretiens d'embauche.

## Matériel

Un laptop professionel est fourni avec un support fabricant. Le support est en général sur site avec pièce et main d'oeuvre, le fdabricant a donc un intérêt direct à produire des produit fiables. Les laptop du commerce pour particulier n'ont pas vraiment cette contrainte.

Pour travailler confortablement il faut disposer d'un CPU relativement moderne avec 4 coeurs et 8GB de ram. suivant le système d'exploitation et la charge associé il peu être necessaire de monter à 16 ou 32 GB de ram.

Je vous recommande les sites de vente en ligne de matériel professionel reconditionné. Pour un prix relativement faible vous disposerez d'un outil de travail fiable et suffisament puissant.

## Les systèmes

Pour être claire, dans notre proffession savoir utiliser Linux n'est pas une option. Cependant pour des raisons organisationnel il est trés courant de devoir travailler en environnement hostile (sous windows sans droits d'admin).

**Utilisation de Linux** : mon choix personnel, il me permet d'avoir un niveau de maitrise important de mon outil de travail. Cela necessite en revanche des connaissances et une bonne organisation.

avantages : l'intégration des outils open source est implicite, une fois les principe de fonctionn ement bien assimillé il est possible de maitriser complètement son OS.

inconvénients : Des logiciels ne sont pas disponible sous linux. il faut en général faire sois même l'administration de son laptop.

**Utilisation de windows** : intuitif et clef en main il conviens à la plupart des utilisateurs. Sa maitrise n'est pourtant pas du tout évidente car on s'en remet à l'éditeur (et a l'administrateur).

avantages : étant une référence international, quasiement tout les logiciels sont disponible sous windows. La possibilité de déléger de l'administration de son OS a une entité interne à l'entreprise.

inconvénients : l'intégration des logiciel open source est parfois complexe. La délégation d'administration vas à l'encontre de la maitrise.

**dual boot** : Disposer des deux systèmes d'exploitation est un plus en revanche devoir rebooter son laptop n'est pas du tout efficace, on préfèrera disposer d'une VM avec l'autre OS afin de pouvoir utiliser les deux en même temps.

**Utilisation de mac OS** : intuitif et clef en main il conviens à la plupart des utilisateurs.

avantages : C'est un Unix, fiable robuste, la pluparty des les logiciels sont disponible sous macOS.

inconvénients : l'intégration des logiciel open source est parfois complexe. le prix

## Gestion de l'outil de travail

### Les mise à jours

Le premier mardi du mois, windows se met à jour, evitez de prévoir une intervention ou une réunion ce jour là ou anticiper les impacts

Sous linux vous pouvez planifier vos mise à jour comme vous l'entendez.

Dans tous les cas, vous devez gèrer les mise à jour.

### Gestion des sauvegardes

En cas de panne majeure (exemple : rupture du disque dure) vous vous devez de reduite au minimum la durée de votre chômage technique.

C'est à dire :

* Avoir un système qui boot
* Récupèrer tous les logiciels nécessaire
* remettre en place vos configurations personnelles
* restorer vos donner personnels

> Vous disposez d'un grand nombre de compte sur des applications, site web etc... Un gestionaire de mots de passe est un outils indispensable, sa restoration un impératif.

Certains outils de sauvegarde permettent de répondre complètement à ce besoin mais cela à en général un coût.

**Vous devez mettre en place la solution de sauvegarde qui vous conviens**
