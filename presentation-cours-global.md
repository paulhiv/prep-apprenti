# Cours de gestion des infrastructures informatiques

## Contexte

Dans le cadre d'un apprentissage, il convient de valider et compléter les acquis professionnels et techniques afin de pouvoir se présenter à un employeur potentiel avec des compétences acquises et confirmées et un projet professionel précis.

La formation ici présentée se propose de compléter une formation Bac+2 en informatique. L'objectif est de favoriser l'intégration des étudiants en apprentissage dans le domaine de la gestion des infrastructures informatiques.

### Pré-requis attendus

* Disposer des connaissances techniques en système et réseau au niveau Bac+2
* Savoir lire l'anglais technique
* **Être curieux, motivé et sérieux**

### Organisation et méthodologie

Une alternance de journée de cours suivis d'une journée de travail en autonomie, complète, sur le même chapitre.

#### Journée de cours

Les journées de cours viendront compléter les acquis de l'étudiant *via* :

* Des rappels théoriques et pratiques
* La réalisation de travaux dirigés
* La présentation de cas professionels
* La présentation de bonnes pratiques organisationnelles et techniques

#### Journées en autonomie

Les journées en autonomie permettent à l'étudiant de revoir et intégrer les compétences abordées la veille en (suivant les sujets abordés) :

* Réalisant des travaux pratiques sur les problématiques présentées
* Mettant en oeuvre des plateformes techniques
* Avançant sur leur projet individuel (la solution retenue par l'étudiant)
* Mettant en oeuvre les sujets présentés, la veille, sur leur projet individuel

#### Objectifs pédagogiques

Ce cours a pour objet d'aider les étudiants à :

* Mieux connaître les attentes professionnelles du métier
* Valider et compléter les acquis théoriques et pratiques
* S'engager dans son projet professionnel
* Mettre en oeuvre des plateformes d'infrastructure

Outre l'attitude professionnelle abordée de façon transversale, l'étudiant devra valider un ensemble de compétences abordées dans des modules de cours. Modules présentés plus loin dans ce document.

#### Méthodes d'évaluations

La validation des acquis sera effectuée par :

* Des QCM rapides de quelques questions en début et fin de cours
* La vérification des réalisations des travaux dirigés en cours et des travaux pratiques réalisés en autonomie
* La validation de l'avancement sur le projet individuel

## Description des modules et chapitres abordés

### Module : Connaissances du métier

#### Gestion d'une Infrastucture IT

* Les services et organisations de l'informatique
* Maintien en condition opérationnelle : la gestion des évènements, incidents, problèmes et changements suivant l'`ITIL`
* Outillage des processus de maintien en condition opérationnelle (ticketing, monitoring, sauvegarde, snapshots, gestion des configurations, etc... )
* Présentation d'une architecture pour une plateforme de services

__Objectifs :__

* Valider ses connaissances en culture générale autour de l'informatique et des métiers liés à la gestion des infrastructures informatiques
* Avoir une attitude professionelle, maîtriser son périmètre de responsabilité et d'intervention
* Savoir faire de la veille technologique
* Être capable de décrire une architecture de plateforme de service

__temps :__ 4h

#### Outils de travail incontournables

* Gestion de son système d'exploitation
* Sauvegarde de ses données
* Les accès distants
* Versionning avec git
* Virtualisation système sur son ordinateur personnel

__Objectifs :__

* Savoir gérer et planifier la gestion de son ordinateur
* Savoir assurer une pérennité de la donnée au travers des sauvegarde et de l'utilisation des processus et outils
* Savoir utiliser les outils `git`, `ssh`, `rdesktop`
* Être capable de préparer et valider ses opérations système sur un lab virtualisé (partie système)

__temps :__ 4h

__Travail en autonomie sur ce module :__

* Remise aux normes de son ordinateur personnel
* Mise en place de sauvegardes automatisées de son laptop
* Veille technologique : choix d'un outils à intégrer (ticketing, sauvegarde, monitoring, services internet, etc...) : __la solution retenue par l'étudiant__
* Definition de l'architecture de plateforme de services pour l'outil choisi

__Ces sujets organisationnels seront aussi abordés de façon transversale avec les sujets techniques décrits ci-après.__

---

### Module : Système et intégration d'application

#### Ligne de commande et scripting

* Les commandes Unix et le `man`
* Les langages de scripting (`bash`, `sed`, `awk`, `python`, `perl`, `php` etc...)
* Les bonnes pratiques de scripting
* La gestion des processus
* La gestion des droits sous Unix et GNU/linux

__Objectifs :__

* Savoir comprendre et utiliser à bon escient les ressources internet et tutoriel
* Être capable de créer des scripts maintenables et réutilisables
* Comprendre et disposer des bonnes pratiques en terme de gestion de droits

__temps :__ 8h

__Travail en autonomie sur ce chapitre :__

* Ecriture de scripts `bash` `python` et `awk`

#### Administration système 1

* Installation système et partitionement
* Configuration réseau et stockage
* Intégration d'applications
* Les logs et le troubleshooting

__Objectifs :__

* Comprendre le partitionnement
* Savoir identifier les informations nécessaires à l'installation et la configuration d'un système
* Comprendre la gestion de *packages* et de *repositories* d'une distribution
* Appréhender le principe d'intégration d'une application
* Savoir analyser les logs, analyser et résoudre des anomalies

__temps :__ 8h

__Travail en autonomie sur ce chapitre :__

* Déploiement de templates de VM Linux `debian`, `CentOS` et `Ubuntu server`
* Installation et test de __la solution retenue par l'étudiant__

#### Administration système 2

* Outils de gestion des configurations `ansible`
* Sauvegarde

__Objectifs :__

* Appréhender la gestion des configurations et __l'infrastructure as code__ avec `ansible` et `vagrant`
* Savoir identifier les différents éléments d'une architecture à sauvegarder

__temps :__ 8h

__Travail en autonomie sur ce chapitre :__

* Intégration de __la solution retenue par l'étudiant__ avec `ansible`.
* Mise en place de sauvegardes des données vivantes de __la solution retenue par l'étudiant__

#### Principaux services internet

* Présentation `Vagrant`
* Domain Name Services (`DNS`)
* Serveur web `HTTP`
* Service de messagerie `SMTP`

__Objectifs :__

* Connaître le fonctionnement et la gestion des principaux services internet
* Être capable d'analyser et résoudre des incidents de services internet

__temps :__ 12h

__Travail en autonomie sur ce chapitre :__

* Intégration de __la solution retenue par l'étudiant__ dans une infrastructure internet complète (`dns`, `smtp`, `http`)

#### Gestion de la disponibilté 

* Configuration avancée réseaux sous Linux (`802.1q`, `802.3ad`)
* Configuration avancée du stockage sous Linux (`raid` et `LVM`)
* `Keepalived` et haute disponibilité des services réseaux
* Loadbalancing applicatif (web avec `nginx`)

__Objectifs :__

* Appréhender la gestion de la haute disponibilité
* Savoir identifier les points uniques de défaillance d'une architecture
* Savoir mettre en place de la redondance sur un système
* Savoir rendre une application web hautement disponible

__temps :__ 12h

__Travail en autonomie sur ce chapitre :__

* mise en haute disponibilité de __la solution retenue par l'étudiant__

---

### Module : Réseau

#### Switching

* Reprise des bases du réseau, validation des acquis
* Présentation de l'outil `"Packet tracer"`
* Configurer un réseau commuté
* Comprendre les concepts de bases du switching

__Objectifs:__

* Savoir segmenter son réseau
* Savoir utiliser des outils de maquettage et se familiariser avec la __configuration des équipements__
* Comprendre les concepts de la commutation réseau (`ARP`, `Broadcast`, `Multicast`...)
* Appréhender et comprendre les __protocoles courants__ (`STP`, `802.3ad`, `802.1q`)
* Prévoir une panne, mettre en place de la __redondance__ dans une architecture

__temps:__ 8h

__Travail en autonomie sur ce module :__

* Mise en place d'un réseau Multi-Vlan `"Router On a Stick"`
* Mise en place de protocoles afin d'__optimiser__ la bande passante et de garantir la pérénité des liaisons
* __Contourner les problématiques__ liées aux protocoles historiques
* Customisation d'une architecture de niveau 2 en plusieurs étapes 

#### Securité

* Un firewall dans une architecture
* Les protocoles de transport
* Qu'est ce qu'un `UTM` ?

__Objectifs__:

* Savoir __configurer un Firewall__ en coeur de réseau d'une architecture
* Pouvoir __se protéger__ contre les intrusions & inspecter le trafic au sein de son entreprise
* Être capable de __publier des services__ sur le Net __de façon sécurisée__
* Mettre à disposition des collaborateurs un accès distant pour travailler
* Savoir __interconnecter des sites__ sans réseau privé

__temps:__ 8h

__Travail en autonomie sur ce chapitre :__

* __Déploiement d'un Firewall__ sous forme de machine virtuelle
* Utilisation du firewall en tant que Core Router
* Mise en place de protocoles d'`inspection SSL`, `Antivirus`, `Filtrage Web (URL & DNS)`, `IPS`
* Configuration de __VPN__ `SSL` et `IPSec`
* Utilisation du `DNat` et du `SNat`

#### Routage

* Les concepts du routage IP
* Le troubleshooting de base
* Les différents protocoles dynamiques
* La redondance de couche 3
* L'encapsulation de trames
* La mutualisation des routeurs

__Objectifs:__

* Savoir mettre en place un réseau routé
* Maîtriser les techniques de __troubleshooting__ basiques, adopter les bons réflexes
* Appréhender les differents __protocoles de routage dynamique__
* Être capable de __redonder__ son infrastructure

__temps:__ 8h

__Travail en autonomie sur ce chapitre :__

* Configuration d'un réseau complexe multi-routeurs de type __backbone opérateur__
* Mise en place d'une __redondance complète__ du réseau
* Création des `VRFs` sur les équipements
* Deploiement d'une __configuration BGP optimisée__ (`Route redistribute`, `Route Map`, `BFD`, `Route Target`, `Route Distinguishers`...)
* Application de __tunnels `MPLS`__ sur l'infrastructure finale

#### Professionalisation, de la théorie à la pratique

* Le __quotidien__ en entreprise
* Les __différentes entreprises__ et leurs __besoins__ (topologies réseau typiques)
* La gestion du matériel
* La gestion des configurations

__Objectifs:__

* __Être efficace__ au sein d'une entreprise en ayant une __vision professionnelle__
* Comprendre et interpréter le besoin
* Se préparer aux __cas atypiques__ mais récurrents
* __Gérer__ son parc matériel
* __Maîtriser__ la configuration des équipements

__temps:__ 8h

__Travail en autonomie sur ce chapitre :__

* Optimisation et finalisation du *TP Routing*

---

### Module : Les Infrastructures de production

#### Infrastructure Wifi et VoIP

* Présentation des technologies les plus courantes
* Configuration WiFi côté infrastructure et côté client
* Présentation des technologies VoIP
* Utilisation et configuration VoIP

__Objectifs :__

* Comprendre les technologies sans-fil et leurs limitations
* Savoir identifier les informations nécessaires à l'installation et la configuration d'un système WiFi
* Comprendre les technologies de voix sur IP et leurs utilisations
* Savoir identifier les informations nécessaires à l'installation et la configuration d'un système VoIP
* Maîtriser les prérequis techniques d'un projet VoIP (VLAN, QoS)
* Comprendre les enjeux de la téléphonie en entreprise

__temps :__ 8h

__Travail en autonomie sur ce chapitre :__

* Déploiement d'une configuration Wifi "Aruba"
* Déploiement d'une configuration cliente WIfi (GPO AD)
* Déploiement d'une solution de téléphonie sur IP (Asterisk)

#### La virtualisation de serveurs

* La virtualisation en entreprise
* La haute disponibilité des infrastructures
* Présentation des produits du marché (`VMware`, `Microsoft`, `Citrix`)
* Configuration d'un système de virtualisation simple (`vSphere` ou `Hyper-V` au choix)
* Introduction au stockage d'entreprise, les grandes tendances (`SAN`, `NAS`, `HyperConvergence`)

__Objectifs :__

* Comprende les grands principes de la virtualisation et ses atouts
* Savoir identifier les informations nécessaires à l'installation et à la configuration d'un système de virtualisation
* Connaître les principes de fonctionnement du stockage en entreprise 
* Être capable d'exploiter une solution de virtualisation

__temps :__ 8h

__Travail en autonomie sur ce chapitre :__

* Intégration de __la solution retenue par l'étudiant__ 
* Réalisation de tâches d'administration
* Analyse de performances et diagnostique simple

#### Stockage et sauvegarde

* Présentation des solutions de stockage d'entreprise
* Présentation de la sauvegarde en entreprise - intérêts et enjeux
* Présentation des outils : `Commvault` et `Veeam`
* La sauvegarde des équipements actifs (orientée réseau)

__Objectifs :__

* Savoir identifier les informations nécessaires à un projet stockage
* Comprendre les mécanismes de sauvegarde et les contraintes techniques et organisationnelles 
* Appréhender la gestion des configurations réseau
* Comprendre les méthodologies de déploiement et de pilotage d'un projet de sauvegarde
* Savoir créer des jobs de sauvegarde

__temps :__ 8h

__Travail en autonomie sur ce chapitre :__

* Intégration d'une solution de sauvegarde de configuration réseau (`Rancid`)
* Intégration d'une solution de sauvegarde d'entreprise (`Veeam`)

#### Supervision

* Présentation de la supervision en entreprise (SLA etc)
* Présentation de quelques outils du marché (`Centreon`, `Nagios` et dérivés...)

__Objectifs :__

* Connaître les différentes méthodologies de supervision (active/passive)
* Savoir exploiter un outil de supervision courant (Nagios/Centreon)
* Appréhender la supervision de performance applicative

__temps :__ 8h

__Travail en autonomie sur ce chapitre :__

* Intégration d'une solution de supervision (au choix)
* Création d'un dashboard de supervision (outil au choix)
* Création d'un rapport de disponibilité
