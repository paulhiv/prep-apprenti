# virtualisation pour du lab système

## Les Outils

VirtualBox, VmWare workstation, KVM (linux), parallels desktop (mac)

Pour la suite du cours il sera indispenssable de disposer de **VirtualBox** sur votre machine.

## Principe

Un processus exécuté sur votre système dispose d'accès directe à des ressources (CPU RAM) et périphérique (disque, console, carte réseaux) virtuels. Il peu alors exécuter un système d'exploitation complet. C'est une machine virtuelle.

Du point de vue de votre laptop (l'hyperviseur) c'est un seul processus, depuis la machine virtuel c'est un système complet pouvant exécutant d'autre processus.

## Les fonctionalités ajouté

en plus de pouvoir disposer de pluseurs système d'exploitation qui s'exécute simultanément sur une seul machine physique et les autre gains lié à la mutiualisation des ressources, la virtualisation à apporté énormément de souplesse dans la gesion des système d'informations.

### Les snapshots

Il est possible de Fixer un état dans le temps de la machine virtuel puis de revenir à cet état. Trés util dans les travaux d'étude et pour un laboratoir systeme, cette fontionnalité apporte aussi beaucoup en production nottament pour faciliter les retour arrière sur des exécution de changements.

snapshot de disque : Copy on write et redirect on write

Nous avons :

* l'image disque : un espace de stockage fini qui représente un disque
* le snapshot : l'image du disque figé sur sa version à la date du snapshot
* le live : Limage du disque courant

#### copy on write

Ou COW, lors d'une écriture sur un bloc de données, ce bloc live est copié dans un fichier dédier au snapshot, puis est modifier sur le live. le vloc est copier pour sauvegarde à l'écriture : Copy On Write.

l'image disque contiens alors la version live de l'image disque, le snapshot dispose d'un espace disque dédié sur lequel on sauvegarde la version des bloc **avant** la date du snapshot.

#### redirect on write

Ou ROW, lors d'une écriture sur un bloc l'écriture est redirigé vers un fichier dédié au live et le bloc initiale n'est pas modifié.

l'image disque contiens alors le snapshot, le live dispose d'un espace disque dédié qui ne contiens que les version des blocs modifié **après** snapshot.

### Les clones

Un clone est une copie de machine virtuel : Sa configuration et ses images disque sont duppliqués, les adresse mac des carte réseaux sont en général regénéré afin d'éviter les conflits

**clones complets** : les disques sont intégralement recopier sur disque

**clones liés** : chacune des machine virtuel dispose alors d'une version live du disque basé sur un unique snapshot.

