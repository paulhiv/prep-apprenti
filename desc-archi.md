# description d'une infrastructure

## Objectif de la solution

On décrit brièvement les enjeux de la mise en oeuvre de cette solution et les éventuelles métriques récupèrer dans l'expression de besoin.

## Inventaire technologique

On recence ici les technologies matériel et logiciels mises en oeuvre tant physique que logique en y associant les éventuels contrats de support et les principes de gestion des anomalies associée.

Cette partie est rédigé par l'architecte de la solution.

## Implantation physique

On retrouve ici toute les informations nécessaires à l'équipe datacenter pour la mise en place de la solution. Dans le cadre de la mise en oeuvre de l'infrastructure. 

* L'inventaire matériel
* Les positionements dans les racks et les consomations électriques associées.
* Les arrivées réseaux attendues

Cette section ne sera nécessaire au déploiement de la solution peu être rédiger par le prestataire (interne ou externe) responsable de l'intégration matériel.

## Architecture Réseaux

### Présentation

Il conviens de présenter les différents Vlan, subnet et éventuelement les VRF à mettre en oeuvre avec une description de ceux-ci. Un shéma de principe réseaux (par zone réseaux) est attendu ici

### Implantation logique

On précise alors le schéma précédent en précisant les valeurs associées aux vlans et subnet (et VRF) cette partie est séparré car rédiger en regard de l''existant.

### Implantation physique

A destination de l'équipe datacenter et rédigé par l'intégrateur matériel ou l'équipe réseaux on précise ici les connexions réseaux attendu pour tous les équipements réseaux.

## Architecture système

### Le stockage

Si existant les éléments autonome de stockage (nas san) devront être présenter avec les principes d'usages :

* Classe de stockage (besoin en performances)
* Gestion des capacité et de la croissance
* Méthode de sauvegardes

### Les roles

Décrire pour chaque host son role et pour chaque role les éléments communs à tout les hosts de ce role parmis les données suivantes :

* ressources : modèle, cpu, ram, disques et carte controleurs, carte réseaux...
* Données logiques :
  * Système d'exploitation
  * Gestion du stockage et modèle de partitionning et filesystèmes
  * Description des Connexions réseaux attendues par cartes réseaux
  * package logiciels `socle` à installer

### les fiches hosts

host par host on précise alors les valeures attendues pour les instanciation du role : id-inventaire, nom, ip, wwn etc...

Ces informations nécessaires à la mise en oeuvre seront alors saisie dans les outils d'inventaire d'exploitation.

## Architecture logicielle

Les équipes d'intégration logiciel précise ici par host ou par role les logiciels et configurations attendu.

* Les services réseaux externe à la solution (DNS, smtp etc...)
* Les logiciels `socle` et `applicatifs`
* Les flux réseaux applicatifs (la matrice des flux)

## Architecture fonctionnelle

Enfin on précise de façon simplifiée le modèle fonctionnel de la solution et les flux métiers associé à l'infrastruture.

Ce dernier point est lié à l'ingénieurie logiciel mais constitue une source d'information indispenssable aux équipe d'exploitation et de support de la solution.

## En conclusion

Un besoin métier est défini pour y répondre on défini les point ci dessus du dernier (architecture fonctionnel) au premier (implantation physique de l'inventaire matériel) au cours du projet de conception. En revanche le projet de déploiement lui suit le dossier dans le sens inverse.

Sur la présentation de la solution viendra s'ajouté la notion d'environnement : les instanciation / implantation sont effectué pour chaque environnement :

* environnement de developpement, lab et/ou test
* environnement de recette et/ou de pre-production
* environnement de production.

A ce document il conviendra d'ajouter les documents d'exploitation :

* comment démarrer, arreter, sauvegarder, restaurer, monitorer la solution
* les procédure opérationnelles courantes (Création/suppression d'une nouvelle instance de tel ou tel objet fonctionnel Exemple : ajout/suppression d'utilisateurs)
* Le plan de production et l'odonnancement des traitements.

[Exemple de description d'une architecture complexe](https://www.cisco.com/c/en/us/td/docs/unified_computing/ucs/UCS_CVDs/cisco_ucs_xd77esxi60u1_flexpod.html)
