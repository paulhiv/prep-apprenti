# Gestion d'une Infrastucture IT

## Infrastructure IT

__Definition :__
L'ensemble des composants matériels et logiciels assujetti à la fourniture des services de gestion de l'information d'une entitée. Leur configuration, les moyens d'hébergement associés ainsi que les outils permettant leur gestion ; composent l'infrastucture de gestion de l'information de cette entité.

Cela comprend :

* Le datacenter ou la salle blanche avec les arrivées électriques et la climatisation.
* Le matériel acheté ou loué ansi que les contrats de support associés à ceux-ci.
  * Les alimentation electrique et onduleurs
  * réseaux switch routeur firewall
  * Serveurs et matériels dédié (Exemple : pour le stockage)
  * les cables réseaux fibre et électriques
  * Les postes de travails des utilisateurs
* Les systèmes d'exploitation les logiciels ainsi que les droits de licences et support associées.
* Les instances applicatives
* Les configurations matériels, logiciel et applicatives
* La donnée métier

L'entité produit `des services métier` (vente de chocolat, facturation des consomation des clients d'un opérateur de téléphonie, etc... ), ces service métier repose sur des services de technologie de l'information: des aplications en production.

## L'organisation de la DSI

### Les métiers de la DSI

#### L'hébergement

Gestion :

* Du contrat avec le datacenter ou de la salle blanche
* Les accès physiques
* Du matériels : reception des livraison, gestion du stock, gestion du support matériel, rackage, occupation et capacité des baies (electrique, espace, etc...)

#### Ingénieurie : Réseaux, Stockage, Systèmes et Virtualisation

* Gestion de l'architecture et de ces évolutions
* Gestion des configurations et du déploiement de celles-ci
* Gestion des sauvegardes
  * Suivi des plan de sauvegardes et des échecs
  * Externalisation des sauvegardes
* Gestion du monitoring
  * déploiements
* Gestion des référentiels documentaire et des procédures
* Gestion des contrats fournisseurs et de leur support
* Suivi des capacités

#### L'intégration

* La gestion projets
  * Planification, coordination, suivi des actions et des coûts
* Gestion des dévelopements
  * Gestion des bugs et des évolutions logiciels
* Automatisation et gestion des déploiement applicatifs
* Ordonancement des taches automatisé
* Gestion des bases de données

#### Le support

Le support est un service fourni aux utilisateurs du systeme d'information c'est le point d'entrée de la DSI. Il permet à ces derniers de formuler des demandes et de déclarer des anomalies.

Cette activité est en général transverse à toute la DSI répartie par niveau de support

Exemple :

* Niveau 1 : Prise d'appels, monitoring, analyse de premier niveau, résolution et traitement simple et en général procédurée
* Niveau 2 : Analyse et résolution d'anomalies non procèdurée
* Niveau 3 : escalades des anomalies et demandes complexe à l'ingénieurie

#### La production métier

C'est l'utilisation du système d'information pour produire les `services métier`. Certaines Equipes sont dédié à ces activités.

### Les Processus

#### Gestion des Alertes et des remontés

La gestion des évènements, des alertes ou des rémontés est souvent rattaché au `support` (niveau 1) ; Lié au alertes de monitoring et aux remontés des utilisateurs (déclaration d'incidents).
L'objectif de ce processus est de s'assurer que les `évènements` sont pris en comptes et traité de bout en bout.

#### Gestion des Incidents

Souvent associé au `support` (niveau 2), La gestion des incidents ou des anomalies consiste à réduire au minimum le temps d'indisponibilité des services métier et à garantir `le niveau de service attendu`.

Un incident est une indisponibilité avéré ou prédite d'un service métier, ou plus simplement d'une `anomalie de conformité`.

L'objectif est de remettre le service métier 'en route' au plus tot ou déviter son blocage.

Exemple :

* Relance de processus système
* Reprise de processus métier
* Augmentation de capacité de certaines ressources
* Application de solution de contournement sur des `problèmes` connues

__Niveau de service attendu__ (SLA): il s'agit de la définition de la disponibilité souahité pour tel ou tel service. Exemple disponibilité à 99,98% de 8h à 20h
cf : https://fr.wikipedia.org/wiki/Service-level_agreement

#### Gestion des problèmes

Un probleme est la reprodution d'incidents récurents de même type elle peu être aussi associée à une prédiction d'incident majeure.
L'objectif de la gestion de probleme est de définir la source des incidents récurents puis de proposer des solutions permettant de `résoudre le problème`.

Dans un grand nombre de cas la gestion de problème propose des solutions de contournements ; procèdures utilisées dans la cadre de la gestion d'incident pour traiter les occurences des ces anomalie. Dans ce cas l'objectif est alors de permettre de vivre avec le problème.

#### Gestion des Changements

La gestion des changements est le processus permettant de garantir la continuité de la production métier ou la disponibilité des services métier au cours de l'application des modifications de l'infrastructure du système d'information.

Les changements sont classifié :

* les changements standards sont des opérations procédurées et maitrisées.
* Les changements non standards sont des demandes nécessitant une analyse impliquant un étude d'impact avant réalisation.
* Les changement d'infrastructure necessite de passer par le CAB.

__Le CAB__ : `Change advisory board` ou commité consultatif des changements. Ce commité est consulté afin de valider, plannifier et organiser la réalisation des modifications de l'infrastructure.

Un changements est présenté au CAB de la façon suivante : 

présentation de :

* L'objectif du changement (le risque à ne pas faire)
* Le risque associés à la réalisation du changement (le risque à faire)
* Le planning, le plan d'action et les indisponibilités associées
* Le plan de communication associé au changement
* Le plan de retour arrière (pour annuler le changement)

#### Gestion des Capacités

Ce processus à pour objectif d'éviter les saturation des ressources limité en quantité ou volume. Il s'agit de suivre les consomation et de planifier les `changements` nécessaire afin d'éviter les saturations.

### Les bases de connaissances

#### La doc

La documentation est obsolète, incomplète, éronnée mais nécessaire.

* Documentation fournisseur et editeur
  * Documentations et procédures produit
  * Les interfaces de support
    * Interface web, compte, numéro de contrat etc...
    * contact mail ou téléphonique pour escalades
* Documentation technique interne
  * Dossier d'architecture,
  * Dossier d'exploitation,
  * **Les matrices de flux**

Considèrez la documentation comme une lettre à votre "moi" future.

> Un wiki !

#### Les référentiels

* Inventaires de gestion des actif de l'infrastucture
  * Matériel et support matériel
  * Licences logicielles
  * Certificats ssl
  * Nom de domaines

* Référenciel techniques
  * DCIM : Data Center Infrastructure Managment
  * IPAM : IP adress managment (vlan, ip, subnet)
  * Les VMs

#### Le ticketing

Les systèmes de ticketing permettent de tracer les opérations, communications relatives aux demandes, incidents, déploiement et problèmes. 

c'est un élément essenciel de la base de connaissance car il permet de retrouver, via les métadonnées des ticket, tout les évènement incident changement lié à un device ou a une des plateforme de service.
