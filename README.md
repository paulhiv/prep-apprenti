# Préparation à l'apprentissage

## Module : Connaissances Métier

[présentation du module](./connaissances-metier.md)

### Gestion du système d'information

Le `run` ou l'exploitation : [Principes de gestion d'une production](./gestion-IT.md)

Le `build` ou la conception : [description d'une solution d'infrastructure](./desc-archi.md)

### Outils a maitriser

* [Son laptop](./laptop.md)
* [accès distant](./ssh.md)
* [git](./git.md)
* [virtualisation sur laptop](./virtu-laptop.md)

### Travaux pratiques sur le module

#### Travaux individuel

* Revoir le cours et preparrer d'eventuelles questions.
* Gestion saine de votre laptop (si ce n'est pas déja le cas)
  * Assurez vous que votre laptop est un outil de travail fiable (et donc sauvegardé, mise à jours, procédure de reconstruction complète)
  * choisissez et installez un outil de gestion de mot de passe et un outil ssh (si besoin)
* Créez votre identité ssh et git :
  * Créer vous une paire des clef ssh sécurisé par un mot de passe
  * Créez-vous un compte sur gitlab associé à votre clef ssh
  * Créez vous une config git associé a votre compte gitlab
* Péparrez une présentation très rapide de votre outil de travail.
* Choisir une solution technique à mettre en place.

#### Travail Collaboratif

Vous allez produire les procédures de configuration initiale de vm : CentOS 7, Debian 9 et 10 , Ubuntu server 18.04 :

* Vous realiserer une VM "template" par distribution.
* Vous cloner ces VM et effectuer la configuration initiale des VM : IP statiques, nom de VM, domaine (lab.local) durcissement ssh
* Vous effectue en makdown la documentation associée a ces VM :
  * Vous justifierez des choix de configuration initiale des templates (partitionning taille des disque ram CPU carte réseaux)
  * Vous décriver la procédure de configuration initiale

Vous pousserez votre code markdown sur le dépot git de ce cours dans un nouveau dossier config-linux via **une seule merge request**, contenant les commits bien commenté de chacun des étudiants

---

## Module : Système et intégration d'application

### Ligne de commande et scripting

#### la ligne de commande

* Une présentation du [shell](./shell.md)
* Le [man](./man.md)
* Les principales [commandes](./commands.md) et un [TD](./TD-commands.md)
* Les commandes [filtres](./filtres.md) et encore un [TD](./TD-filtres.md)
* les filtres avec [awk](./awk.md)

#### l'environement utilisateur et les droits

* L'[environnement](./environment-Unix.md) d'execution du shell et un [TD](./TD-environment.md)
* Gestion des comptes [utilisateurs](./utilisateurs.md) et un [TD](./TD-utilisateurs.md)
* Gestion des [Droits](./droits.md) sur les fichiers et encore un [TD](./TD-droits.md)
* La gestion des [processus](./processus.md)

#### l'arborescence de fichiers

* L'[arborescence](./arborescence.md) de fichiers
* Comment [rechercher](./find.md) des fichiers dans une arborescence avec la commande `find` et son [TD](./TD-find.md)

#### Le scripting

* Les langages de scripting (`bash`, `sed`, `awk`, `python`, `perl`, `php`, `powershell` etc...)
* [scripting bash](./scripting.md) et son [TD](./TD-scripting.md)

### Travaux pratiques sur ce chapitre

* Vous créez un script de sauvegarde d'une base de données de type mysql avec l'outil mysqldump.
  * votre script source le fichier $HOME/.backupconf de l'utilisateur courant. Celui-ci contiens les variables d'environnement nécessaire au backup de la base de données. Le script sera réutilisable par une autre utilisateur.
  * votre script permet de conserver un certains nombre de versions et purge automatiquement les sauvegardes antérieur
  * votre script gère les erreures et quite donc avec un code retour non nul en cas de probleme sur la sauvegarde
  * un fichier de log conservera les trace des actions
* Imaginez une solution permettant de sauvegarder plusieurs base de données avec le même compte unix (donc un seul fichier backup conf)
* Ecrivez la procédure de restauration des bases
